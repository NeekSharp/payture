﻿using System;
using NUnit.Framework;

namespace Payture.Test
{
    [TestFixture]
    public class PayMethodTest
    {
        [TestCase("Merchant", "257627403355252356023543631402773212",(ulong)100,true)]
        public void PayMethod_Validate(string key,string orderid, ulong amound,bool result)
        {
            PayInfo payInfo = GetPayInfo();
            PayMethod payMethod = new PayMethod(key, payInfo, orderid, amound);

            Assert.IsTrue(payMethod.IsValid() == result);
        }

        private PayInfo GetPayInfo()
        {
            return new PayInfo("4111111111111112", "03", "18", "Ivan Ivanov", "123", "257627403355252356023543631402773212", (ulong)100);
        }
    }
}
