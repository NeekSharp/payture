﻿using System;
using NUnit.Framework;


namespace Payture.Test
{
    [TestFixture]
    public class PayInfoTest
    {
        [TestCase("4111111111111112","03","18", "Ivan Ivanov", "123", "257627403355252356023543631402773212", (ulong)100.00, true)]
        public void PayInfo_Validate(string pan, string eMonth, string eYear, string cardHolder, string secureCode, string orderId, ulong amount, bool isValid)
        {
            PayInfo info = new PayInfo(pan,eMonth,eYear,cardHolder,secureCode,orderId,amount);
            Assert.IsTrue(info.IsValid() == isValid);
        }

        [TestCase("4111111111111112", "03", "18", "Ivan Ivanov", "123", "257627403355252356023543631402773212", (ulong)100.00,
            "PAN=4111111111111112;EMonth=03;EYear=18;CardHolder=Ivan Ivanov;SecureCode=123;OrderId=257627403355252356023543631402773212;Amount=100;")]
        public void PayInfo_ToString(string pan, string eMonth, string eYear, string cardHolder, string secureCode, string orderId, ulong amount, string result)
        {
            PayInfo info = new PayInfo(pan, eMonth, eYear, cardHolder, secureCode, orderId, amount);
            Console.WriteLine(info.ToString());
            Console.WriteLine(result);
            Assert.IsTrue(info.ToString() == result);
        }
    }
}
