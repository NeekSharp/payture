﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Payture
{
    public class PayMethod
    {

        [Required]
        public string Key { get; private set; }

        [Required]
        public PayInfo PayInfo { get; private set; }
        
        [Required]
        [StringLength(50)]
        public string OrderId { get; private set; }

        [Required]
        [CustomValidation(typeof(PayMethod), "isWhole")]
        public ulong Amount { get; private set; }

        [StringLength(50)]
        public string PaytureId { get; set; }

        [StringLength(50)]
        public string CustomerKey { get; set; }

        public CustomFields CustomFields { get; set; }

        public static ValidationResult isWhole(ulong number)
        {
            ValidationResult result = new ValidationResult("Amount must be whole number");

            if ((number % 1) == 0)
            {
                result = ValidationResult.Success;
            }

            return result;
        }


        public PayMethod(string key, PayInfo payinfo, string orderId, ulong amount)
        {
            Key = key; 
            PayInfo = payinfo; 
            OrderId = orderId;
            Amount = amount;
        }

        public Dictionary<string,string> KeyValueDictionary()
        {

            Dictionary<string, string> parametrs = new Dictionary<string, string>()
            {
                {"Key", Key},
                {"PayInfo", PayInfo.ToString()},
                {"OrderId", OrderId},
                {"Amount", Amount.ToString()},
            };

            // Не понял пока может ли одно из полей использоваться без остальных
            if (PaytureId != null) parametrs.Add("PaytureId", PaytureId);
            if (CustomerKey != null) parametrs.Add("CustomerKey", CustomerKey);
            if (CustomFields != null) parametrs.Add("CustomFields", CustomFields.ToString());

            return parametrs;
        }

        public bool IsValid()
        {
            var validationResults = new List<ValidationResult>();
            var context = new ValidationContext(this);
            bool result = false;

            if (!Validator.TryValidateObject(this, context, validationResults, true))
            {
                foreach (var error in validationResults)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
    }
}
