﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payture
{
    class OperationResult
    {
        // <Pay OrderId = "257627403355252356023543631402773212" Key="Merchant" Success="False" ErrCode="DUPLICATE_ORDER_ID" />
        // <Pay OrderId="370105454370674172340603401650555102" Key="Merchant" Success="True" Amount="100"/>
        public string OrderId { get; set; }
        public string Key { get; set; }
        public bool Success { get; set; }
        public ulong Amount { get; set; }
        // Можно заменять при чтении на описание ошибок
        public string ErrCode { get; set; }

    
        public OperationResult Parse(Dictionary<string,string> data)
        {
            Amount = 0;
            ErrCode = String.Empty;
            foreach (KeyValuePair<string,string> element in data)
            {
                switch (element.Key.ToLower())
                {
                    case "orderid":
                        {
                            OrderId = element.Value;
                            break;
                        }
                    case "key":{
                            Key = element.Value;
                            break;
                        }
                    case "success":
                        {
                            Success = Boolean.Parse(element.Value);
                            break;
                        }
                    case "amount":
                        {
                            Amount = ulong.Parse(element.Value);
                            break;
                        }
                    case "errcode":
                        {
                            ErrCode = element.Value;
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }

            return this;
        }
    }
}
