﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Payture
{
    public class CustomFields
    {
        [Required]
        [StringLength(15)]
        [RegularExpression(@"^(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b")]
        public string IP { get; private set; }

        [Required]
        public string Description { get; private set; }

        public CustomFields(string ip, string description)
        {
            IP = ip;
            Description = description;
        }

        public bool IsValid()
        {
            var validationResults = new List<ValidationResult>();
            var context = new ValidationContext(this);
            bool result = false;

            if (!Validator.TryValidateObject(this, context, validationResults, true))
            {
                foreach (var error in validationResults)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("IP=" + HttpUtility.UrlEncode(IP) + ";");
            sb.Append("Description=" + HttpUtility.UrlEncode(Description) + ";");

            return sb.ToString();
        }
    }
}
