﻿using System;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Payture.Test
{
    [TestFixture]
    public class ApiTest
    {
        [Test]
        public void Api_RuestedUri()
        {
            Api api = new Api("sandbox");
            Uri uri = api.GenerateUri(GetPayMethod());
            Console.WriteLine(uri.AbsoluteUri);
        }

        [Test]
        public async Task Api_GetResponse()
        {
            Api api = new Api("sandbox");
            OperationResult response = await api.Pay(GetPayMethod());
            
            Assert.IsTrue(response.OrderId == "257627403355252356023543631402773212");
        }


        private PayInfo GetPayInfo()
        {
            return new PayInfo("4111111111111112", "03", "18", "Ivan Ivanov", "123", "257627403355252356023543631402773212", (ulong)100);
        }

        private PayMethod GetPayMethod()
        {
            PayMethod payMethod = new PayMethod("Merchant", GetPayInfo(), "257627403355252356023543631402773212", (ulong)100);
            payMethod.CustomFields = GetCustomFields();

            return payMethod;
        }

        private CustomFields GetCustomFields()
        {
            return new CustomFields("7.2.236.72", "MyTestTransaction");
        }
    }
}
