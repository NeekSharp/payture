﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payture
{
    class PaytureApiException : Exception
    {
        public PaytureApiException() { }

        public PaytureApiException(string message) : base(message) { }

        public PaytureApiException(string message, Exception inner) : base(message, inner) { }

        protected PaytureApiException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
