﻿using System;
using NUnit.Framework;

namespace Payture.Test
{
    [TestFixture]
    class CommonFieldsTest
    {
        [TestCase("192.168.0.1", "wqe", true)]
        public void CommonFields_Validate(string ip, string descr, bool isValid)
        {
            CustomFields customFields = new CustomFields(ip, descr);
            Assert.IsTrue(customFields.IsValid() == isValid);
        }

        [TestCase("7.2.236.72", "MyTestTransaction", "IP=7.2.236.72;Description=MyTestTransaction;")]
        public void CommonFields_ToString(string ip, string descr, string correctResult)
        {
            CustomFields customFields = new CustomFields(ip, descr);
            Console.WriteLine(customFields.ToString());
            Console.WriteLine(correctResult);
            Assert.IsTrue(customFields.ToString() == correctResult);
        }
    }
}
