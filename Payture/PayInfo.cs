﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Payture
{
    public class PayInfo
    {

        [Required]
        // TODO: Посмотерть их реальные ограничения
        // Пока из таблицы https://en.wikipedia.org/wiki/Payment_card_number
        [RegularExpression(@"\d{12,19}")]
        public string PAN { get; private set; }

        [Required]
        [RegularExpression("[01][0-9]",ErrorMessage = "Expiration month should match regular expression [01][0-9]")]
        public string EMonth { get; private set; }

        [Required]
        [RegularExpression("[01][0-9]", ErrorMessage = "Expiration year should contains only two last digits")]
        public string EYear { get; private set; }

        [Required]
        [StringLength(30)]
        [RegularExpression(@"^(\s|[\w]+\s[\w]+)$", ErrorMessage = "CardHolder must contains only letters or space")] 
        public string CardHolder { get; private set; }

        [Required]
        [RegularExpression(@"^[\d]{3}$",ErrorMessage = "SecureCode must match CVC2 or CVV2")]
        public string SecureCode { get; private set; }

        [Required]
        [StringLength(50)]
        public string OrderId { get; private set; }

        [Required]
        public ulong Amount { get; private set; }

        public PayInfo(string pan, string eMonth, string eYear, string cardHolder, string secureCode, string orderId, ulong amount)
        {
            PAN = pan;
            EMonth = eMonth;
            EYear = eYear;
            CardHolder = cardHolder;
            SecureCode = secureCode;
            OrderId = orderId;
            Amount = amount;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("PAN=" + PAN + ";");
            sb.Append("EMonth=" + EMonth + ";");
            sb.Append("EYear=" + EYear + ";");
            sb.Append("CardHolder=" + CardHolder + ";");
            sb.Append("SecureCode=" + SecureCode + ";");
            sb.Append("OrderId=" + OrderId + ";");
            sb.Append("Amount=" + Amount.ToString() + ";");

            return sb.ToString();
        }

        public bool IsValid()
        {
            var validationResults = new List<ValidationResult>();
            var context = new ValidationContext(this);
            bool result = false;

            if (!Validator.TryValidateObject(this, context, validationResults, true))
            {
                foreach (var error in validationResults)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
    }
}
