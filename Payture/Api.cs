﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Linq;

namespace Payture
{
    class Api
    {
        Uri _baseUri = null;

        public Api(string host)
        {
            _baseUri = new Uri(String.Format("https://{0}.payture.com/api/Pay", host));
        }

        public async Task<OperationResult> Pay(PayMethod payMethod)
        {
            OperationResult result = new OperationResult();

            if(    payMethod.PayInfo.IsValid() 
                && (payMethod.CustomFields == null || payMethod.CustomFields.IsValid()) 
                && payMethod.IsValid())
            {
                Uri _requestUri = null;
                _requestUri = GenerateUri(payMethod);

                string data = await GetResponse(_requestUri);

                result.Parse(ReadResponse(data));
            }
            else
            {
                throw new PaytureApiException("Запрос не был отправлен по причине не корректных данных запроса");
            }

            return result;
        }

        internal Uri GenerateUri(PayMethod payMethod)
        {
            Uri reqUri = new Uri(_baseUri.AbsoluteUri);
            Dictionary<string, string> parameters = payMethod.KeyValueDictionary();
            
            foreach (var parameter in parameters)
            {
                reqUri = reqUri.AddQuery(parameter.Key, parameter.Value);
            }

            return reqUri;
        }

        internal async Task<string> GetResponse(Uri uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12; // SecurityProtocolType.Tls12 можно вынести куда нибудь на случай изменений
            request.Method = "POST";
            request.ServerCertificateValidationCallback += CertificateValidationCallBack;

            
            string result = null;
            HttpWebResponse response = null;

            try
            {
                using (response = (HttpWebResponse)await request.GetResponseAsync())
                using (System.IO.StreamReader str = new System.IO.StreamReader(response.GetResponseStream(),Encoding.UTF8))
                {
                    result = await str.ReadToEndAsync();
                }
            }
            catch (ProtocolViolationException ex)
            {
                Console.WriteLine("ProtocolViolationException при запросе {0}, {1}", ex.Message);
                throw new PaytureApiException("Возникло исключение при обращнию к API", ex);
            }
            catch (WebException ex)
            {
                Console.WriteLine("WebException при запросе {0}, {1}", ex.Status, ex.Message);
                throw new PaytureApiException("Возникло исключение при обращнию к API", ex);
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
            
            return result;
        }

        private static bool CertificateValidationCallBack( object sender, 
            System.Security.Cryptography.X509Certificates.X509Certificate certificate,
            System.Security.Cryptography.X509Certificates.X509Chain chain,
            System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }
            // Если есть любые ошибки с сертификатом, то все отменяется
            return false;
        }


        internal Dictionary<string, string> ReadResponse(string data)
        {
            Dictionary<string, string> responseData = new Dictionary<string, string>();
            XmlDocument doc = new XmlDocument();
            try { 
                doc.LoadXml(data);
            }
            catch (XmlException ex)
            {
                Console.WriteLine("Ответ не является XML документом. {0}", ex.Message);
                return responseData;
            }

            XmlNode node = doc.FirstChild;
            foreach(XmlAttribute attribute in node.Attributes)
            {
                responseData.Add(attribute.Name.ToLower(), attribute.Value);
            }

            return responseData;
        }



    }
}
