﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Payture.Test")]

namespace Payture
{
    class Program
    {
        static void Main(string[] args)
        {
            PayInfo payInfo = new PayInfo("4111111111111112", "03", "18", "Ivan Ivanov", "123", "257627403355252356023543631402773212", 100);
            CustomFields customFields = new CustomFields("192.168.0.1", "test transaction");
            PayMethod payMethod = new PayMethod("Merchant", payInfo, "257627403355252356023543631402773212", 100);

            Api api = new Api("sandbox");

            Task.Run(async () => {
                try { 
                    OperationResult result = await api.Pay(payMethod);
                    Console.WriteLine(result.Key);
                    Console.WriteLine(result.Success);
                    Console.WriteLine(result.OrderId);
                    Console.WriteLine(result.Amount);
                    Console.WriteLine(result.ErrCode);
                }
                catch(PaytureApiException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            });

            Console.ReadLine();
        }
    }
}
